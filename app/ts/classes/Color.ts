import { Tools } from "./Tools";

export class Color {

    private r: number;
    private g: number;
    private b: number;
    private a: number;

    constructor( r = 0, g = 0, b = 0, a = 1) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    random() {

        this.r = Tools.getRandomInt(0, 255);
        this.g = Tools.getRandomInt(0, 255);
        this.b = Tools.getRandomInt(0, 255);

        return this;
    }

    toRGBA() {
        return `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`;
    }

}