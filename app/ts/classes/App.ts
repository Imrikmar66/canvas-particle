import { Circle } from "./Circle";

class App {

    // constante de classe en typescript
    readonly MAX_SIZE = 400;

    public $canvas: HTMLCanvasElement;
    public ctx: CanvasRenderingContext2D;

    // public circles: Array<Circle>
    public circles: Circle[] = [];

    constructor() {
        // Le 'as' ou <> permettent de forcer le type
        // this.$canvas = <HTMLCanvasElement>document.getElementById('canvas');
        this.$canvas = document.getElementById('canvas') as HTMLCanvasElement;
        this.resizeCanvas();

        this.ctx = this.$canvas.getContext('2d') as CanvasRenderingContext2D;
    }

    free() {
        if( this.circles.length > this.MAX_SIZE )
            this.circles.shift();
    }

    addCircle( circle: Circle ): void {
        this.circles.push( circle );
    }

    removeCircle( key: number ): void {
        this.circles.splice( key, 1 );
    }

    private resizeCanvas(): void {

        this.$canvas.height = window.innerHeight;
        this.$canvas.width = window.innerWidth;

    }

    public clear(): void {
        this.ctx.clearRect( 
            0, 
            0, 
            this.$canvas.width, 
            this.$canvas.height 
        );
    }

}

export default new App;