import { Point } from "./Point";
import { Tools } from "./Tools";
import { Color } from "./Color";

export class Circle {

    public position: Point;
    public velocity: Point;

    private rayon: number;
    private line: number;

    private fcolor: Color;
    private scolor: Color;

    private bounce: number = 3;

    constructor( 
        position: Point, 
        rayon: number, 
        line: number, 
        fcolor: Color, 
        scolor: Color 
    ) {

        this.position = position;
        this.rayon = rayon;
        this.line = line;
        this.fcolor = fcolor;
        this.scolor = scolor;

        this.velocity = new Point(
            Tools.getRandomFloat(-5, 5),
            Tools.getRandomFloat(-5, 5)
        );

    }

    getRayon(): number {
        return this.rayon;
    }

    draw( ctx: CanvasRenderingContext2D ): void {

        ctx.beginPath();
        ctx.arc( 
            this.position.x, 
            this.position.y, 
            this.rayon, 
            0, 
            2 * Math.PI 
        );
        
        ctx.fillStyle = this.fcolor.toRGBA();
        ctx.fill();
        
        ctx.lineWidth = this.line;
        ctx.strokeStyle = this.scolor.toRGBA();
        ctx.stroke();

    }

    move() {
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
    }

    checkLimit( top_left: Point, bottom_right: Point ) {

        if( this.position.x <= top_left.x + this.rayon ) {
            this.velocity.x = Math.abs( this.velocity.x );
            this.bounce --;
        }
        else if( this.position.x >= bottom_right.x - this.rayon ) {
            this.velocity.x = -Math.abs( this.velocity.x );
            this.bounce --;
        }
        
        if( this.position.y <= top_left.y + this.rayon ) {
            this.velocity.y = Math.abs( this.velocity.y );
            this.bounce --;
        }
        else if( this.position.y >= bottom_right.y - this.rayon ) {
            this.velocity.y = -Math.abs( this.velocity.y );
            this.bounce --;
        }

    }

    endBounce(): boolean {
        return this.bounce <= 0;
    }

}