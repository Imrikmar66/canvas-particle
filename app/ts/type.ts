let a: number = 2;
let b: string = 'test';
let c: boolean = true;

let d: number[] = [1, 2, 25, 0.21];
let dbis: Array<number> = [2, 3, 4, 56];

let e: string[] = ['test', 'truc'];
let ebis: Array<string> = ['chaine'];

let f:{ keya: number, keyb: string } = {
    keya: 12,
    keyb: 'str'
};

// Objet js dont les clés sont des string et les valeurs des number
let g: { [key: string]: number } = {
    a: 78,
    b: 1.2
};

let l: {} = {
    a: 12,
    b:'truc',
    c: true
};

let h: string|number = 'my string';
h = 48;

let i: Array<string|number|boolean> = [12, 'salut', true];
let j: (string|number|boolean)[] = [ 13, 'au revoir', false, 'azeaz', 12, 45 ];

class OneClass {

    public property1: number;
    protected property2: number;
    private property3: number;

    private $dom: HTMLElement;

    constructor( property1: number, property2: number, property3: number ) {
        this.property1 = property1;
        this.property2 = property2;
        this.property3 = property3;
    }

    public method1(): void {
        this.$dom.innerText = 'salut';
        this.$dom.innerHTML = '';
    }

    protected method2(): void {

    }

    private method3(): void {

    }

}

interface Init {
    init(): void;
}
interface OnDestroy {
    destroy(): void;
}

let m: OneClass = new OneClass(12, 13, 14);

class SecondClass extends OneClass implements Init, OnDestroy {
    
    private property4: string;
    
    constructor( p1: number, p2: number, p3: number, p4: string ) {
        super( p1, p2, p3 );
        this.property4 = p4;
    }
    
    destroy(): void {

    }
    init(): void {
        
    }

}

let n: SecondClass = new SecondClass(1, 2, 3, 'test');
n.method1();
n.destroy();

let o: OneClass = new SecondClass(1, 2, 3, 'test');
o.method1();
