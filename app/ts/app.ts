import '../scss/styles';
import app from './classes/App';
import { Circle } from './classes/Circle';
import { Point } from './classes/Point';
import { Color } from './classes/Color';

app.$canvas.addEventListener('mousemove', (event) => {

    for( let i = 0; i < 30; i++ ) {

        const position: Point = new Point(
            event.pageX,
            event.pageY
        );

        const circle = new Circle(
            position,
            5,
            3,
            (new Color).random(),
            (new Color).random()
        );

        app.free();

        app.addCircle( circle );
        
    }

});

const top_left = new Point(0, 0);
const bottom_right = new Point(
    app.$canvas.width,
    app.$canvas.height 
);

function animate() {

    // On efface l'écran
    app.clear();
    
    for( let key in app.circles ) {

        const circle = app.circles[key];

        // On déplace le cercle
        circle.move();
        
        // On vérifie les bordures
        circle.checkLimit( top_left, bottom_right );

        if( circle.endBounce() ) {

            app.removeCircle( parseInt( key ) );

        }
        
    }

    for( let circle of app.circles ) {
        // On redessine
        circle.draw( app.ctx );
    }
    
    // On rappelle le tout avec la fonction 
    // d'animation du navigateur
    requestAnimationFrame( animate );
}

// On lance la fonction une première fois
animate();